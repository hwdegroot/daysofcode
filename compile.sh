#/usr/bin/env bash
cd `dirname $0`

image=$(docker run \
  -t \
  -d \
  -v `pwd`:`pwd` \
  -w `pwd` \
  -u `id -u`:`id -g` \
  --entrypoint cat \
  ruby:2-alpine)
docker exec -tu root $image gem install sass -v 3.4.2
docker exec -tu root $image apk add --update nodejs
docker exec -tu root $image npm install -g uglify-js
docker exec -t $image rm -f public/css/daysofcode.css public/js/custom.js
docker exec -t $image mkdir -p public/css/bgimg public/js
docker exec -t $image cp app/bgimg/unicorn.png public/css/bgimg/
docker exec -t $image uglifyjs app/assets/js/custom.js --beautify --source-map  --output public/js/custom.js
#docker exec -t $image cp app/assets/js/custom.js public/js/custom.js
docker exec -t $image sass --update --scss --sourcemap=inline --unix-newlines app/assets/scss:public/css
docker stop --time 0 $image
