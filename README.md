[![build status](https://gitlab.com/hwdegroot/daysofcode/badges/master/build.svg)](https://gitlab.com/hwdegroot/daysofcode/commits/master)

## Usage

This is the source for the presentation given at [daysofcode Rotterdam](http://www.daysofcode.nl/), July 23rd, 2017.

View the presentation in action [here](https://hwdegroot.gitlab.io/daysofcode).

## Slide notes

To show the slide notes, run the presentation with query parameter `show-notes=true`

    /path/to/presentation?show-notes=true

## Acknowledgements

This presentation was made with [reveal.js](https://github.com/hakimel/reveal.js)
