(function(){
  "use strict";

  document.querySelectorAll("img[title^='Courtesy:']").forEach(function(img) {
    var title = img.getAttribute("title");
    var caption = document.createElement("div");
    var wrapper = document.createElement("div");
    var newImg = img.cloneNode(true);
    wrapper.setAttribute("class", "wrapper");
    caption.setAttribute("class", "caption");
    caption.innerHTML = title;
    newImg.removeAttribute("title");
    wrapper.appendChild(newImg);
    wrapper.appendChild(caption);
    img.replaceWith(wrapper);
  });

})();

function getQueryParameter(param, url) {
  "use strict";

  if (!url) url = window.location.href;

  var name = param.replace(/[\[\]]/g, "\\$&");
  var re = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
  var res = re.exec(url);
  return (!res || !res[2]) ? null : decodeURIComponent(res[2].replace(/\+/g, " "));
}
